config.assignId = false;
config.comp = {
	on: false,
	prefixes: ['-webkit-', '-moz-', '']
}
let ice = new Presentation();

let slide1 = new Slide();
slide1.addStyle('righted');
slide1.addStyle('top');
slide1.chState(0);
slide1.pushElement(
	'Free Software',
	'',
	'text-align: right;\
	color: black;\
	font-size: 120px;\
	padding-top: 120px;\
	padding-right: 20px;'
);
slide1.pushElement(
	'Artur Gulik',
	'',
	'color: black;\
	font-size: 30px;\
	text-align: right;\
	padding-right: 20px;');

let slide2 = new Slide();
slide2.setTitle(
	'What is Free Software?',
	'text-align: center;\
	font-size: 90px;');
slide2.adjust = true;
slide2.addStyle('centered');
slide2.addStyle('middle');

function nextImgState(num, ourSlide, imageURLs){
		ourSlide.chState(num+1); //we want to leave state 0 without images
		let last;
		if (num === 4) {
			last = true;
			num = 3;
		} else {
			last = false;
		}
		ourSlide.chSection(3); ourSlide.chSection(num);
		let imgFromStyle, imgToStyle;
		for (let i=0; i<=num; ++i){
			ourSlide.chSection(i);
			imgFromStyle = 'width: 90%; height: 90%;';
			imgToStyle = 'width: 90%; height: 90%;';
			if (i === num) {
				if (last) imgFromStyle = 'opacity: 1;';
				else {
					imgFromStyle += ' opacity: 0;';
					imgToStyle += ' opacity: 1;';
				}
			}
			else if (i === num-1) {
				if (last) imgFromStyle = 'opacity: 0.4;';
				else {
					imgFromStyle += ' opacity: 1;';
					imgToStyle += ' opacity: 0.4;';
				}
			}
			else {
				if (last) imgFromStyle = 'opacity: 0.4;';
				else {
					imgFromStyle += ' opacity: 0.4;';
					imgToStyle += ' opacity: 0.4;';
				}
			}
			if (last) {//last function call
				imgToStyle = 'width: 90%; height: 90%; opacity: 0;';
			}
			ourSlide.pushElement(
				imageURLs[i],
				imgFromStyle,
				imgToStyle);
		}
}
slide2.setSubTitle('\
\“Free software\” means software that respects users\' freedom and community.\<br>\
<span style="display: block; text-align: right;">- gnu.org</span>\
 ','display: block; margin-top: 40px; margin-left: 50px; margin-right: 50px; font-size: 40px;');

let urls = [];
for(let i=0;i<4;++i) urls.push(`img=./img/four_freedoms_${i}.png`);
nextImgState(0, slide2, urls);
nextImgState(1, slide2, urls);
nextImgState(2, slide2, urls);
nextImgState(3, slide2, urls);
nextImgState(4, slide2, urls);

slide2.chSection(0);
slide2.chState(6);
//here
slide2.addContainerStyle('full-height');
slide2.addSectionStyle('full-width');
let is = 'width: 100%;height: 100%;';
slide2.pushElement('img=https://static.fsf.org/nosvn/appeal2020/spring/6-freedoms.png',
										is + 'opacity: 0;',is + 'opacity: 1;');

let slide3 = new Slide();
slide3.setTitle(
	'What is it not?',
	'text-align: center;\
	font-size: 90px;');
slide3.addStyle('centered');
slide3.addStyle('middle');

let common = 'position: relative;display:inline-block;color:white;font-size: 85px;'
slide3.chState(1);

slide3.pushElement('Freeware',
	common + 'opacity: 0;',
	common + 'opacity: 1;'
);
slide3.pushElement('Open-source',
	'',
	common + 'opacity: 0;'
);

//---------------
slide3.chState(2);

slide3.pushElement('Freeware',
	'',
	common,
	'strike'
);
slide3.pushElement('Open-source',
	'',
	common + 'opacity: 0;'
);

slide3.chState(3);
slide3.pushElement('Freeware',
	common + 'transform: translateY(0);',
	common + 'transform: translateY(-60%);',
	'striked'
);
slide3.pushElement('Open-source',
	common + 'opacity: 0;transform: translateY(100%);',
	common + 'animation-delay: 0.3s;opacity: 1;transform: translateY(0);'
);

slide3.chState(4);
slide3.pushElement('Freeware',
	'',
	common + 'transform: translateY(-60%);',
	'striked'
);
slide3.pushElement('Open-source',
	'',
	common,
	'strike'
);

let slide4 = new Slide();
slide4.setTitle(
	'Why bother?',
	'text-align: center;\
	font-size: 90px;');
slide4.setSubTitle(
	'It\'s not just a way of licensing. It\'s a philosophy',
	'text-align: center;\
	display: block;\
	margin-top: 40px;\
	margin-left: 50px;\
	margin-right: 50px;\
	font-size: 40px;');
slide4.addStyle('centered');
slide4.addStyle('middle');

common = 'position: relative;display:inline-block;color:white;font-size: 60px;'
let text = ['Collaboration', 'Security', 'Privacy', 'Customizability'];
let hide = 'opacity: 0;';
let show = 'opacity: 1;';
let correct = -80;
let movement = 80;
let showDelay = '0.3s';

let delay = `animation-delay: ${showDelay};`;
for (let i=0;i<text.length;++i){
	slide4.chState(i+1); //we're leaving state 1 empty
	for (let j=0;j<text.length;++j){
		//console.log(`i: ${i} | j: ${j}`);
		if (i === j && i === 0){
			let placed = `transform: translateY(${(-movement*(i-j))+correct}%);`;
			slide4.pushElement(text[j], common + hide + placed, common + show + placed);
			//console.log('adding =');
			continue;
		}
		if (i === j){
			let beforePlaced = `transform: translateY(${(-movement*(i-j-1))+correct}%);`;
			let placed = `transform: translateY(${(-movement*(i-j))+correct}%);`;
			slide4.pushElement(text[j], common + hide + beforePlaced, common + show + placed + delay);
			//console.log('adding =');
			continue;
		}
		if (j > i){
			slide4.pushElement(text[j], '', common + hide);
			//console.log('adding >');
			continue;
		}
		if (j < i){
			let lastplaced = 'transform: translateY(${correct}%);';
			if (i-j-1 > -1)
				lastplaced = `transform: translateY(${(-movement*(i-j-1))+correct}%);`;
			let placed = `transform: translateY(${(-movement*(i-j))+correct}%);`;
			//console.log('adding <');
			slide4.pushElement(text[j], common + lastplaced, common + placed);
		}
	}
}

let slide5 = new Slide();
slide5.setTitle(
	'Free Software examples',
	'text-align: center;\
	font-size: 90px;');
slide5.setSubTitle('','','');
slide5.addStyle('centered');
slide5.addStyle('middle');
slide5.adjust = true;
slide5.addContainerStyle('full-height');
slide5.addContainerStyle('full-width');

slide5.addSectionStyle('full-width');

//urls = [];
//for(let i=0;i<4;++i) urls.push(`img=./img/four_freedoms_${i}.png`);
urls = ['img=https://www.mozilla.org/media/protocol/img/logos/firefox/browser/logo.eb1324e44442.svg', 'img=https://download.blender.org/branding/blender_logo.png', 'img=https://upload.wikimedia.org/wikipedia/commons/e/e6/VLC_Icon.svg', 'img=https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/OBS_Studio_Logo.svg/512px-OBS_Studio_Logo.svg.png'];
nextImgState(0, slide5, urls);
nextImgState(1, slide5, urls);
nextImgState(2, slide5, urls);
nextImgState(3, slide5, urls);
//nextImgState(4, slide5, urls);


let slide6 = new Slide();
slide6.setTitle(
	'More Free Software examples',
	'text-align: center;\
	font-size: 90px;');
slide6.setSubTitle('','','');
slide6.addStyle('centered');
slide6.addStyle('middle');
slide6.adjust = true;
slide6.addContainerStyle('full-height');
slide6.addContainerStyle('full-width');

slide6.addSectionStyle('full-width');

urls=['img=https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Inkscape_Logo.svg/480px-Inkscape_Logo.svg.png','img=https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/The_GIMP_icon_-_gnome.svg/512px-The_GIMP_icon_-_gnome.svg.png','img=https://upload.wikimedia.org/wikipedia/commons/7/73/Calligrakrita-base.svg','img=https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Tux.svg/400px-Tux.svg.png'];

nextImgState(0, slide6, urls);
nextImgState(1, slide6, urls);
nextImgState(2, slide6, urls);
nextImgState(3, slide6, urls);
//nextImgState(4, slide6, urls);

//---

let slide7 = new Slide();

slide7.setSubTitle('','','');
slide7.addStyle('centered');
slide7.addStyle('middle');
slide7.chSection(0);
slide7.chState(0);
slide7.pushElement('What now?', '','position: relative;display:inline-block;color: white;font-size: 85px;');



let slide8 = new Slide();

slide8.setSubTitle('','','');
slide8.addStyle('centered');
slide8.addStyle('middle');
slide8.chSection(0);
slide8.chState(0);
slide8.pushElement('Thank you!', '','position: relative;display:inline-block;color: white;font-size: 85px;');
slide8.pushElement('Artur Gulik', '','position: relative;display:inline-block;color: white;margin-top: 35px;font-size: 35px;');
// --------------------------
ice.slides = [slide1, slide2, slide3, slide4, slide5, slide6, slide7, slide8];

ice.setBackgrounds(['img/africa_namibia_nature_dry_2.jpg', '', '','', '','','','']);

slide1.createHTML();
slide2.createHTML();
slide3.createHTML();
slide4.createHTML();
slide5.createHTML();
slide6.createHTML();
slide7.createHTML();
slide8.createHTML();
ice.init();




//A slide is a Slide object with two lists of states (for and back States)

