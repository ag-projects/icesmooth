"use strict";

let loader = document.getElementById('loader');
window.addEventListener("load", () => {
	loader.style.transition = "opacity 1s ease";
	loader.style.opacity = 0;
	setTimeout(() => {
		loader.style.display = 'none';
	}, 1000);
});

class Presentation{
	constructor(slides){
		this.slideCounter = 0;
		this.slide = 0;
		this.state = 0;
		this.elementCount = 0;
		this.animationCount = 0;
		this.elements = [];
		this.slides = [];
	}
	async setBackgrounds(sources){
		let promises = [];
		let slides = this.slides;
		sources.forEach(function(source, index){
			promises.push(new Promise(function(resolve) {
				let img = new Image();
				img.onload = function(){
					slides[index].slideDiv.style.backgroundImage = 'url('+source+')';
					resolve();
				};
				img.src = source;
			}));
		});
		await Promise.all(promises);
	}
	hide(){
		let current = this.slides[this.slide];
		current.forStates[this.state].forEach(function(element){
			element.hide();
		});
		current.backStates[this.state].forEach(function(element){
			element.hide();
		});
	}

	left(){
		if (this.state > 0 || this.slide > 0){
			--this.state;
			if (this.state < 0) {
				//if we changed to the previous slide
				this.slides[this.slide].hideRight();
				--this.slide;
				this.state = this.slides[this.slide].forStates.length-1;
				this.slides[this.slide].display();
			}
			else {
				//if we changed to the previous state
				this.slides[this.slide].forStates[this.state+1].hide();
				/*
				++this.state;
				let current = this.slides[this.slide];
				current.forStates[this.state].forEach(function(element){
					element.re();
				});
				//backStates should be used here if we want them
				--this.state;
				*/
				//backStates should be used here if we want them

				this.slides[this.slide].forStates[this.state].show();
				/*
				this.slides[this.slide].forStates[this.state]
				.forEach(function(element){
					element.fresh();
				});
				*/
				//this.slides[this.slide].display();
			}
		}
	}
	right(){
		if ((this.state + 1 < this.slides[this.slide].forStates.length) ||
				(this.slide +1 < this.slides.length))
		{
			++this.state;
			if (this.state === this.slides[this.slide].forStates.length) {
				//if we changed to the next slide
				this.slides[this.slide].hideLeft();
				++this.slide;
				this.state = 0;
				this.slides[this.slide].display();
			}
			else {
				//if we changed to the next state
				/*
				--this.state;
				this.hide();
				++this.state;
				this.fresh();
				*/
				this.slides[this.slide].forStates[this.state-1].hide();
				this.slides[this.slide].forStates[this.state].show();
			}
		}
	}
	init(){
		this.slide = 0;
		this.state = 0;
		//for now we need to prepare state0 of each slide
		this.slides.forEach(function(slide){
			slide.hideRight();
			if (slide.forStates[0])
			slide.forStates[0].show();
		})
		this.slides[this.slide].display();
		this.adjustSlides();
	}
	adjustSlides(){
		this.slides.forEach(slide => {
			slide.adjustStateHeights();
		});
	}
}

function keyPress(event){
	let str;
	switch (event.key) {
		case "ArrowLeft":
			ice.left();
		break;
		case "ArrowRight":
			ice.right();
		break;
		case "ArrowUp":
			str = 'Up';
		break;
		case "ArrowDown":
			str = 'Down';
		break;
		case 'f':
			if (document.fullscreenElement ||
			document.webkitFullscreenElement ||
			document.mozFullscreenElement ||
			document.msFullscreenElement){
				document.exitFullscreen()
				.then(() => {
					//console.log('backjusting');
					ice.adjustSlides();
				});
			}
			else
			document.documentElement.requestFullscreen()
			.then(() => ice.adjustSlides())
			.catch((e)=>{
				console.log(e);
			});
		break;
	}
}
document.addEventListener('keydown', keyPress);

class Element{
	constructor(content, cssFrom, cssTo){
		this.id = ice.elementCount++;
		if (content.search(/img=/) === 0){
			this.type = 'img';
			this.src = content.substring(4);
		}
		else {
			this.type = 'span';
			this.content = content;
		}
		this.dom = document.createElement(this.type);
		if (this.content) this.dom.textContent = this.content;
		else this.dom.src = this.src;
		this.dom.style.cssText = cssTo;

		if (cssFrom === '') return;
		this.animName = 'animation' + ice.animationCount++;
		this.animDura = '1s';
		this.animFillMode = 'both';

		let prefixes;
		if (config.comp.on) {
			if (config.comp.prefixes)
				prefixes = config.comp.prefixes;
			else
				prefixes = ['-webkit-', '-moz-', '-ms-', '-o-', ''];
		}
		else prefixes = [''];

		let css = '';
		for (const prefix of prefixes){
			css += `${prefix}animation-name: ${this.animName};\
							${prefix}animation-fill-mode: ${this.animFillMode};\
							${prefix}animation-duration: ${this.animDura};`
		}
		this.dom.style.cssText += css;

		css = '';
		for (const prefix of prefixes){
			css +=
			`@${prefix}keyframes ${this.animName}{\
			from {${cssFrom}}\
			to {${cssTo}}\
			}`;
		}
		let style = document.createElement('style');
		style.type = 'text/css';
		document.head.appendChild(style);
		if (style.styleSheet){
			style.styleSheet.cssText = css;
		}
		else{
			style.appendChild(document.createTextNode(css));
		}
	}
	addStyle(styleClass){
		this.dom.classList.add(styleClass);
	}
	hide(){
		console.log('hiding: ' + this.dom.textContent);
		this.dom.style.display = 'none';
	}
	re(){
		this.dom.style.display = 'none';
	}
	fresh(){
		this.dom.style.removeProperty('display');
	}
}

class Slide{
	constructor(){
		this.slideDiv = document.createElement('div');
		this.forStates = [];
		this.backStates = [];
		this.backgrounds = [];
		//sections is a 2D array ([state][section]) of objects
		//example object: {width: '50%', styles=['top','someStyleClass'], containerStyles=[]}
		this.sections = [];
		this.stateDivs = [];
		this.build = {
			state: 0,
			section: 0
		};
		this.title = 0;
		this.subTitle = 0;
		this.adjust = false;
	}
	addStyle(styleClass){
		this.slideDiv.classList.add(styleClass);
	}
	setTitle(content, style){
		this.title = {
			content: content,
			style: style
		};
	}
	setSubTitle(content, style){
		this.subTitle = {
			content: content,
			style: style
		};
	}
	setSectionWidth(width){
		const {state, section} = this.build;
		while (state > this.sections.length-1)
			this.sections.push([]);
		while (section > this.sections[state].length-1)
			this.sections[state].push([]);
		if (!this.sections[state][section]) this.sections[state][section] = {};
		this.sections[state][section].width = width;
	}
	addSectionStyle(styleClass){
		const {state, section} = this.build;
		while (state > this.sections.length-1)
			this.sections.push([]);
		while (section > this.sections[state].length-1)
			this.sections[state].push([]);
		if (!this.sections[state][section].styles) this.sections[state][section].styles = [];
		this.sections[state][section].styles.push(styleClass);
	}
	addContainerStyle(styleClass){
		const {state, section} = this.build;
		while (state > this.sections.length-1)
			this.sections.push([]);
		while (section > this.sections[state].length-1)
			this.sections[state].push([]);
		if (!this.sections[state][section].containerStyles) this.sections[state][section].containerStyles = [];
		this.sections[state][section].containerStyles.push(styleClass);
	}
	updateStates(){
		// This function prepares forStates for adding new elements on state
		// and section from this.build;
		const {state, section} = this.build;
		// this.forStates[state][section] is a location that we might want to
		// push the element to. Let's make sure this location exists.
		while (state > this.forStates.length-1)
			// while our destination state doesn't exist, push new states
			this.forStates.push([]);
		while (section > this.forStates[state].length-1)
			// while our destication section doesn't exist, push new sections
			this.forStates[state].push([]);
	}
	pushElement(content, cssFrom, cssTo, styles){
		let destination = this.forStates[this.build.state][this.build.section];
		destination.push(new Element(content, cssFrom, cssTo));
		if (styles)
			destination[destination.length-1].addStyle(styles);
	}
	adjustStateHeights(){
		if (!this.adjust) return;
		let body = document.body
		let html = document.documentElement;
		let vh = window.visualViewport.height;

		let rectTop = this.subTitle.element.getBoundingClientRect().top;
		let rectHeight = this.subTitle.element.getBoundingClientRect().height;

		let heightValue = vh - rectTop - rectHeight;
		let height = `${heightValue}px`;

		this.stateDivs.forEach(element => {
			element.style.height= height;
			//console.log(`${element.style.height} = ${vh} - ${rectTop} - ${rectHeight}`);
		});
	}
	createHTML() {
		// We need to make sure that every section has its
		// object in this.sections[][] array.
		// This is done within the forEach functions
		let slideHTML = document.createDocumentFragment();

		if (config.assignId)
			this.slideDiv.id = 'slide' + ice.slideCounter++;
		this.slideDiv.classList.add('slide');
		let offset = 0;
		if (this.title) this.title.element = 0;
		if (this.subTitle) this.subTitle.element = 0;
		if (this.title){
			let titleSpan = document.createElement('span');
			titleSpan.textContent = this.title.content;
			titleSpan.style.cssText = this.title.style;
			this.title.element = this.slideDiv.appendChild(titleSpan);
		}
		if (this.subTitle){
			let subTitle = document.createElement('span');
			subTitle.innerHTML = this.subTitle.content;
			subTitle.style.cssText = this.subTitle.style;
			this.subTitle.element = this.slideDiv.appendChild(subTitle);
		}

		let self = this;
		this.forStates.forEach((state, stateIndex) => {
			if (!self.sections[stateIndex]) self.sections.push([]);
			let stateDiv = document.createElement('div');
			if (config.assignId)
				stateDiv.id = 'sl' + (ice.slideCounter-1) + 'st' + stateIndex;
			stateDiv.classList.add('state');
			state.show = () => stateDiv.style.display='inline-block';
			state.hide = () => stateDiv.style.display='none';

			this.stateDivs.push(this.slideDiv.appendChild(stateDiv));
			//
			state.forEach((section, sectionIndex) => {
				if (!self.sections[stateIndex][sectionIndex]) {
					self.sections[stateIndex].push([]);
					let width = (100/state.length) + '%';
					self.sections[stateIndex][sectionIndex] = {width: width};
				}
				let sectionDiv = document.createElement('div');
				if (config.assignId)
					sectionDiv.id = stateDiv.id + 'se' + sectionIndex;
				if (this.sections[stateIndex][sectionIndex].styles)
				this.sections[stateIndex][sectionIndex].styles.forEach((styleClass) => {
					sectionDiv.classList.add(styleClass);
				});
				sectionDiv.classList.add('section');
				let width = this.sections[stateIndex][sectionIndex].width;
				if (width) sectionDiv.style.width = width;
				let container = document.createElement('div');
				if (config.assignId)
					container.id = sectionDiv.id + 'con';
				if (this.sections[stateIndex][sectionIndex].containerStyles)
				this.sections[stateIndex][sectionIndex].containerStyles.forEach((styleClass) => {
					container.classList.add(styleClass);
				});
				container.classList.add('container');
				sectionDiv.appendChild(container);
				stateDiv.appendChild(sectionDiv);
				section.forEach((element, elementIndex) => {
					let elementDiv = document.createElement('div');
					if (config.assignId)
						elementDiv.id = stateDiv.id + 'el' + elementIndex;
					elementDiv.classList.add('element');

					container.appendChild(elementDiv).appendChild(element.dom);
				});
			});
		});
		slideHTML.appendChild(this.slideDiv);
		document.body.appendChild(slideHTML);
		this.adjustStateHeights();
	}
	chState(n){
		this.build.state = n;
		this.updateStates();
	}
	chSection(n){
		this.build.section = n;
		this.updateStates();
	}

	display(){
		this.slideDiv.style.transform = 'translateX(0)';
	}
	hideLeft(){
		this.slideDiv.style.transform = 'translateX(-100%)';
	}
	hideRight(){
		this.slideDiv.style.transform = 'translateX(100%)';
	}
}
let config = {};
