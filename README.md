[![pipeline status](https://gitlab.com/ag-projects/icesmooth/badges/main/pipeline.svg)](https://gitlab.com/ag-projects/icesmooth/-/commits/main)

# iceSmooth

A web presentation framework. Work in Progress

You can view an example presentation here: [Free Software Presentation](https://arturgulik.gitlab.io/icesmooth)
